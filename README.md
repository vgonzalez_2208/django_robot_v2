# Django_Robot_v2
### Forma parte de trabajo final de grado.

Aplicación web de versión 2 de robot.

## Detalles iniciales

El código aquí mostrado corresponde a una aplicación desarrollada usando el Framework Django sobre el IDE Pycharm de Jetbrains.

Esta aplicación tiene como objetivo servir de interface de usuario gráfica para la interacción con el Robot V2, desde cualquier dispositivo conectado a una red con un navegador Web. Esta interface Web da acceso al usuario de controles de desplazamiento del Robot, información actualizada continuamente de los sensores instalados, herramientas de software para el ajuste de un control PID en los motores y acceso a controles de prueba y puesta en marcha de navegación autónoma a través de entornos.

La aplicación fue desarrollada para una placa de desarrollo Orange Pi Zero con sistema operativo Linux para ARM instalado, sin embargo, esta puede ser desplegada en otro tipo de hardware con soporte para Python que tenga acceso a pines de propósito general (GPIO), como es el caso de la Raspberry Pi o similares.

Para tener un funcionamiento completo de las funciones habilitadas en la aplicación es necesario tener en funcionamiento los algoritmos complementarios de los microcontroladores. Adicional a eso, el hardware donde esté instalada la aplicación debe tener acceso a una red y conocerse las direcciones de acceso ya sea a través de red local o internet.

La documentación del codigo se agregará a medida que surjan dudas de las diversas secciones del proyecto.
