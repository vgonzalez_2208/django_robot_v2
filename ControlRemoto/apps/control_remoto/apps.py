from django.apps import AppConfig


class ControlRemotoConfig(AppConfig):
    name = 'apps.control_remoto'
