from django.http import JsonResponse
from django.views.generic import TemplateView
from django.core.cache import cache

from API_control_remoto.settings import DEBUG
from apps.control_remoto.models import AjusteRuedasPID
from apps.control_remoto.views.control_remoto.functions import *
from apps.control_remoto.views.control_remoto.tests import loop_giro_robot, nav_robot, nav_robot_v2


class ControlRemoto(TemplateView):
    template_name = 'control_remoto/control_remoto_2.html'

    @staticmethod
    def post(request):
        data = {}
        try:
            post = request.POST
            action = post['action']
            if action == 'control_remoto':
                # print(post)
                if post['direction'] == 'adelante':
                    control_r(1, 130)
                elif post['direction'] == 'izquierda':
                    control_r(4, 130)
                elif post['direction'] == 'derecha':
                    control_r(3, 130)
                else:
                    control_r(2, 130)
            elif action == 'data_sensores':
                block_2 = data_sensores()
                data = {
                    'us_1': block_2[2],
                    'us_2': block_2[3],
                    'us_3': block_2[4],
                    'us_4': block_2[5],
                    'us_5': block_2[1],  #
                    'us_6': block_2[0],  #
                    'us_7': block_2[7],
                    'us_8': block_2[6],
                }
                # print(data)
            else:
                data['error'] = 'No ha seleccionado ninguna opción.'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        stop_loops()
        context = super().get_context_data(**kwargs)
        context['dashboard'] = 'active'
        context['debug_value'] = DEBUG
        context['title'] = 'Control Remoto'
        return context


class AjustePID(TemplateView):
    template_name = 'control_remoto/ajuste_pid.html'

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        post = request.POST
        # print(post)
        action = post['action']
        print(action)
        if action == 'lista_test':  # Tabla de pruebas realizadas
            consulta = InfoTest.objects.all()
            data = list(map(lista_test_dt, consulta))
            # print(data)
        elif action == 'ver_test':  # Selección de rueda para ver parámetros
            modelo = post['id_test']
            pos_rueda = post['pos_rueda']
            consulta = AjusteRuedasPID.objects.filter(test_rel=modelo).filter(pos_rueda=pos_rueda)
            data_0 = list(map(gen_info_test, consulta))
            data_0 = ajuste_retraso(data_0)  # Se agrega ajuste de tiempo muerto de 10 ms.
            data_1 = info_global_test(consulta)
            data_1.update(def_ft(consulta))
            data_2 = form_k_rueda(consulta)
            # print(data_1)
            data = [data_0, data_1, data_2]
        elif action == 'borrar_test':
            consulta = InfoTest.objects.get(id=post['id_test'])
            consulta.delete()
            data['status'] = 'ok'
        elif action == 'borrar_todo_test':
            consulta = InfoTest.objects.all()
            consulta.delete()
            data['status'] = 'ok'
        elif action == 'k_pid':
            # print(post)
            flag = check_k_pid_post(post)
            if flag:
                data['status'] = 'ok'
            else:
                data['status'] = 'error'
        elif action == 'select_pr':
            data = lista_ajustes_disp()
            # data_1 = active_pr()
            data['status'] = 'ok'
            # data['info'] = data_0 + data_1
        elif action == 'save_global_pr':
            print(post)
            save_global_pr(post)
            data['status'] = 'ok'
        else:
            data['error'] = 'No ha seleccionado ninguna opción'
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        stop_loops()
        context = super().get_context_data(**kwargs)
        context['ajuste_pid'] = 'active'
        context['debug_value'] = DEBUG
        context['title'] = 'Ajuste PID'
        return context


class NavViewRobot(TemplateView):
    template_name = 'control_remoto/nav_robot.html'

    @staticmethod
    def post(request):
        data = {}
        post = request.POST
        action = post['action']
        print(action)
        if action == 'mov_robot':
            print(post)
            if cache.get('loop_start'):
                cache.set('info_nav', post['info'])
                cache.set('set_nav', True)
            else:
                cache.set('loop_start', True)
                cache.set('set_nav', True)
                cache.set('info_nav', post['info'])
                cache.set('mov_robot', False)
                loop_giro_robot(post)
        elif action == 'stop_loop':
            cache.set('loop_start', False)
            cache.set('nav_start', False)
            cache.set('giro_start', False)
        elif action == 'nav_robot':
            nav_robot_v2()
        else:
            data['error'] = 'No ha seleccionado ninguna opción.'
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        stop_loops()
        context = super().get_context_data(**kwargs)
        context['nav_robot'] = 'active'
        context['debug_value'] = DEBUG
        context['title'] = 'Navegar Entorno'
        return context


def stop_loops():
    cache.set('loop_start', False)
    cache.set('nav_start', False)
    cache.set('giro_start', False)
    cache.set('mov_robot', False)
    cache.set('send_info', False)