import math
import time

from django.core.cache import cache
from smbus2 import SMBus, i2c_msg

# ***********Configuración i2c***************
# from apps.control_remoto.views.control_remoto.tests_functions import *
from apps.control_remoto.models import SelectParametros
from apps.control_remoto.views.control_remoto.tests_functions import Sensores
from apps.control_remoto.ws_functions import get_info_bytes

bus_1 = SMBus(0)  # Bus de xiao y teensy
bus_2 = SMBus(1)  # Bus de fusion IMU
teensy = 0x8
xiao = 0xA
f_imu = 0x28

# ***************Temporizadores***************


# **********Giro del robot *************************


# *****************XIAO_control*********************


# *************PWM*****************************
pwm = 130
data_1 = []


# *********************************************

# ***************Funciones_IMU****************
def leer_euler(data):
    result = [0, 0, 0]
    for i in range(3):
        result[i] = (((data[(i * 2) + 1] & 0xFF) << 8) | (data[(i * 2)] & 0xFF)) & 0xFFFF
        if result[i] & 0x8000:  # > 32767:
            result[i] -= 0x10000  # 65536
    result = [result[0] / 16, result[1] / 16, result[2] / 16]
    return result


def leer_cuaternion(data):
    result = [0, 0, 0, 0]
    for i in range(4):
        result[i] = (((data[(i * 2) + 1] & 0xFF) << 8) | (data[(i * 2)] & 0xFF)) & 0xFFFF
        if result[i] & 0x8000:  # > 32767:
            result[i] -= 0x10000  # 65536
    factor = (1.0 / (1 << 14))
    result = [result[0] * factor, result[1] * factor, result[2] * factor, result[3] * factor]
    return result


# ********** Datos de los sensores*******************
def datos_sensores(data, sensor_us):
    e_v = leer_euler(data)
    # *********Teensy**************
    sensor_us += e_v
    return sensor_us


def loop_giro_robot(post):
    # ***********fusion_IMU**********************
    bus_2.write_byte_data(f_imu, 0x3d, 0)  # Set en CONFIG_MODE
    time.sleep(0.1)
    # time.sleep(1)
    bus_2.write_byte_data(f_imu, 0x3f, 128)  # Set oscilador externo
    # time.sleep(0.2)
    time.sleep(0.8)
    bus_2.write_byte_data(f_imu, 0x3d, 12)  # Set modo NDOF
    time.sleep(0.1)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    # ***********XIAO_MCU**********************
    query = SelectParametros.objects.filter(
        id=1, avance__isnull=False, retroceso__isnull=False, giro_izq__isnull=False, giro_der__isnull=False
    )
    if query:
        info = get_info_bytes(query[0])
    else:
        return 'Error'
    msg = i2c_msg.write(xiao, info)
    bus_1.i2c_rdwr(msg)
    time.sleep(0.1)
    # Test de ajuste de PID
    # bus_1.write_byte(xiao, 3)
    # time.sleep(3)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    inicio = True
    start_move = True
    yaw_final = 0
    imu_array = []
    millis_ini = millis()
    millis_step = millis()
    grados = 0

    while cache.get('loop_start'):
        if cache.get('set_nav'):
            # Giro 90 reloj
            if inicio:
                start_move = 1
                inicio = False
            if cache.get('info_nav') == 'giro_45':
                grados = 45
            elif cache.get('info_nav') == 'giro_90':
                grados = 90
            elif cache.get('info_nav') == 'giro_180':
                grados = 180
            elif cache.get('info_nav') == 'giro_270':
                grados = 270
            elif cache.get('info_nav') == 'giro_270':
                grados = 360
            elif cache.get('info_nav') == 'info_sensores':
                data = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
                sensor_us = bus_1.read_i2c_block_data(teensy, 0, 8)
                vector_s = sensor_us + leer_euler(data)
                print(vector_s)
                start_move = 0
                inicio = True
                cache.set('set_nav', False)
            else:
                grados = 0
                start_move = 0
                inicio = True
                cache.set('set_nav', False)
            if start_move == 1:
                data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
                data_imu = leer_euler(data_imu)
                yaw_ini = data_imu[0]
                yaw_final = yaw_ini + 45
                imu_array = get_imu_array_r(data_imu[0], grados, 4)
                # if yaw_final > 360: yaw_final = yaw_final - 360
                print(imu_array)
                start_move = 2
            elif start_move == 2:
                bus_1.write_i2c_block_data(xiao, 2, [4, 100])
                start_move = 3
            elif start_move == 3:
                data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
                data_imu = leer_euler(data_imu)[0]
                if round(data_imu) == 360: data_imu = 0
                if round(data_imu) in imu_array:
                    val_check = len(imu_array) - imu_array.index(round(data_imu)) <= 12
                else:
                    val_check = True
                # if round(data_imu) not in imu_array or val_check:
                if val_check:
                    bus_1.write_i2c_block_data(xiao, 2, [5, 100])
                    start_move = 4
                else:
                    millis_ini = millis() + 500
                    # millis_step = millis()
            elif start_move == 4:
                if millis_ini <= millis():
                    data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
                    data_imu = leer_euler(data_imu)[0]
                    if int(data_imu) not in imu_array:
                        bus_1.write_i2c_block_data(xiao, 4, [1, 1, 20, 3])
                        millis_ini = millis() + 200
                    elif int(data_imu) != imu_array[-1]:
                        bus_1.write_i2c_block_data(xiao, 4, [1, 1, 20, 4])
                        millis_ini = millis() + 200
                    else:
                        print(data_imu, 'Final mov')
                        start_move = 0
                        inicio = True
                        cache.set('set_nav', False)
            # Giro 90 contra reloj
            elif cache.get('info_nav') == 'nada':
                info_send = [5, 1, 20, 3]  # [cantidad pasos, tiempo pasos, tiempo entre pasos, dirección]
                bus_1.write_i2c_block_data(xiao, 4, info_send)
                print(info_send, 'info enviada')
                cache.set('set_nav', False)
    print('While break')
    bus_1.write_byte(teensy, 0)
    bus_1.write_i2c_block_data(xiao, 2, [5])
    # curses.nocbreak()
    # screen.keypad(False)
    # curses.echo()
    # curses.endwin()


def nav_robot():
    # ***********fusion_IMU**********************
    bus_2.write_byte_data(f_imu, 0x3d, 0)  # Set en CONFIG_MODE
    time.sleep(0.1)
    # time.sleep(1)
    bus_2.write_byte_data(f_imu, 0x3f, 128)  # Set oscilador externo
    # time.sleep(0.2)
    time.sleep(0.8)
    bus_2.write_byte_data(f_imu, 0x3d, 12)  # Set modo NDOF
    time.sleep(0.1)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    # ***********XIAO_MCU**********************
    query = SelectParametros.objects.filter(
        id=1, avance__isnull=False, retroceso__isnull=False, giro_izq__isnull=False, giro_der__isnull=False
    )
    if query:
        info = get_info_bytes(query[0])
    else:
        return 'Error'
    msg = i2c_msg.write(xiao, info)
    bus_1.i2c_rdwr(msg)
    time.sleep(0.1)
    bus_1.write_byte(xiao, 3)
    time.sleep(3)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    inicio = True
    move = False
    yaw_final = 0
    imu_array = []
    millis_ini = millis()
    millis_step = millis()
    estado = 0
    estado_ant = 0
    mov_dir = 0
    c_ang = 0
    sensores_us = 0
    grados = 0
    cache.set('nav_start', True)
    while cache.get('nav_start'):
        if inicio:
            millis_step = millis()
            inicio = False
        # Se inicia con el estado 1
        if estado == 0 and millis_step <= millis():
            sensores_us = bus_1.read_i2c_block_data(teensy, 0, 8)
            # if start_move:
            mov_dir, c_ang = mov_direction(sensores_us, move)
            print(mov_dir, 'es mov_dir', estado_ant, 'estado_ant', move)
            if mov_dir == estado_ant and move:
                estado = 0
                millis_step = millis() + 50
                estado_ant = mov_dir
            elif mov_dir != estado_ant and move:
                estado = 1
                estado_ant = estado
            else:
                estado = mov_dir
        elif estado == 1:
            bus_1.write_i2c_block_data(xiao, 2, [5, 0])  # Se detiene el robot
            if mov_dir != 1:
                estado = mov_dir  # Se aplica el siguiente estado
                estado_ant = estado
            else:
                estado = 0
            millis_step = millis() + 500
            move = False
        elif estado == 2 and millis_step <= millis():
            bus_1.write_i2c_block_data(xiao, 2, [1, 100])  # Avance
            move = True
            estado = 0
            millis_step = millis() + 50
        elif estado == 3 and millis_step <= millis():
            print('estado_3')
            # Giro 90 grados negativo
            cache.set('giro_start', True)
            nav_giro(90 + c_ang, estado)
            estado = 0
            move = True
            millis_step = millis() + 50
        elif estado == 4 and millis_step <= millis():
            print('estado_4')
            # Giro 90 grados positivo
            cache.set('giro_start', True)
            nav_giro(90 + c_ang, estado)
            estado = 0
            move = True
            millis_step = millis() + 50
        elif estado == 5 and millis_step <= millis():
            print('estado_5')
            # Retroceso
            bus_1.write_i2c_block_data(xiao, 2, [2, 100])  # Retroceso
            move = True
            estado = 0
            millis_step = millis() + 50
    bus_1.write_i2c_block_data(xiao, 2, [5, 0])  # Se detiene el robot
    bus_1.write_byte(teensy, 0)  # Se detienen los sensores US


def nav_robot_v2():
    # ***********fusion_IMU**********************
    bus_2.write_byte_data(f_imu, 0x3d, 0)  # Set en CONFIG_MODE
    time.sleep(0.1)
    # time.sleep(1)
    bus_2.write_byte_data(f_imu, 0x3f, 128)  # Set oscilador externo
    # time.sleep(0.2)
    time.sleep(0.8)
    bus_2.write_byte_data(f_imu, 0x3d, 12)  # Set modo NDOF
    time.sleep(0.1)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    # ***********XIAO_MCU**********************
    query = SelectParametros.objects.filter(
        id=1, avance__isnull=False, retroceso__isnull=False, giro_izq__isnull=False, giro_der__isnull=False
    )
    if query:
        info = get_info_bytes(query[0])
    else:
        return 'Error'
    msg = i2c_msg.write(xiao, info)
    bus_1.i2c_rdwr(msg)
    time.sleep(0.1)
    bus_1.write_byte(xiao, 3)
    time.sleep(3)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    inicio = True
    move = False
    yaw_final = 0
    imu_array = []
    millis_ini = millis()
    millis_step = millis()
    estado = 0
    estado_ant = 0
    mov_dir = 0
    c_ang = 0

    # Nuevo algoritmo
    sensores_us = []
    lista_us = [0] * 8
    read_n = 0
    delay_time = 1000  # Tiempo de espera entre estados
    ang_giro = 0
    grados = 0
    check_pos = False
    cache.set('nav_start', True)
    print_f = False
    while cache.get('nav_start'):
        if inicio:
            millis_step = millis()
            inicio = False
        # Etapa inicial de captura de entorno
        elif estado == 0 and millis() >= millis_step:
            if read_n < 5:
                millis_step = millis() + 34  # Tiempo entre lecturas
                sensores_us = bus_1.read_i2c_block_data(teensy, 0, 8)
                # print(sensores_us)
                lista_us = list(zip(lista_us, sensores_us))
                lista_us = list(map(lambda x: x[0] + x[1], lista_us))
                read_n += 1
            else:
                read_n = 0
                millis_step = millis() + delay_time
                # print(lista_us)
                lista_us_avg = list(map(lambda x: x / 5, lista_us))
                # print(lista_us_avg)
                sensor_pr = Sensores(lista_us_avg)
                estado = 1
                lista_us = [0] * 8
                print_f = True
        # Estado de avance
        elif estado == 1 and millis() >= millis_step:
            # Frontal despejado
            # print(sensor_pr.front_r, sensor_pr.front_l)
            # if sensor_pr.dir_info()[0] == 0 and sensor_pr.front_r > 12 and sensor_pr.front_l > 12:
            # print(sensor_pr.dir_info())
            # Se revisa la posición de avance y se comprueba que los sensores frontales obtengan distancias mayores a 12
            if sensor_pr.dir_info().index(2) < sensor_pr.dir_info().index(6):
                if sensor_pr.rear_f > sensor_pr.rear_b:
                    check_pos = True
                elif sensor_pr.rear_f > 12: check_pos = True
                else: check_pos = False
            else:
                if sensor_pr.left_f > sensor_pr.left_b:
                    check_pos = True
                elif sensor_pr.left_f > 12: check_pos = True
                else: check_pos = False
            # Condición de avance frontal
            if sensor_pr.front_r > 12 and sensor_pr.front_l > 12 and check_pos:
                bus_1.write_i2c_block_data(xiao, 2, [1, 80])  # Avance
                millis_step = millis() + 20
                estado = 2
            # Condición de giro
            else:
                cache.set('giro_start', True)
                if sensor_pr.dir_info().index(2) < sensor_pr.dir_info().index(6):
                    nav_giro(30, 3)  # Giro a la izquierda
                # elif sensor_pr.dir_info()[0].index(6) < sensor_pr.dir_info()[0].index(2):
                else:
                    nav_giro(30, 4)  # Giro a la derecha
                estado = 0
                millis_step = millis() + delay_time
            print_f = True
        # Estado de revisión de sensores US
        elif estado == 2 and millis() >= millis_step:
            sensor_us = bus_1.read_i2c_block_data(teensy, 0, 8)
            front_us = [sensor_us[2], sensor_us[1], sensor_us[3], sensor_us[7]]
            front_us = list(filter(lambda x: x <= 12, front_us))
            if len(front_us) != 0:
                estado = 3
            else: millis_step = millis() + 30
        # Estado de parada, stop
        elif estado == 3:
            bus_1.write_i2c_block_data(xiao, 2, [5, 0])
            estado = 0
            millis_step = millis() + delay_time
            print_f = True
        if print_f:
            print(estado, 'es estado')
            print(check_pos, 'es si debe girar o no')
            print(sensor_pr.print_all())
            print_f = False
            # Se inicia con el estado 1
        '''if estado == 0 and millis_step <= millis():
            sensores_us = bus_1.read_i2c_block_data(teensy, 0, 8)
            # if start_move:
            mov_dir, c_ang = mov_direction(sensores_us, move)
            print(mov_dir, 'es mov_dir', estado_ant, 'estado_ant', move)
            if mov_dir == estado_ant and move:
                estado = 0
                millis_step = millis() + 50
                estado_ant = mov_dir
            elif mov_dir != estado_ant and move:
                estado = 1
                estado_ant = estado
            else:
                estado = mov_dir
        elif estado == 1:
            bus_1.write_i2c_block_data(xiao, 2, [5, 0])  # Se detiene el robot
            if mov_dir != 1:
                estado = mov_dir  # Se aplica el siguiente estado
                estado_ant = estado
            else:
                estado = 0
            millis_step = millis() + 500
            move = False
        elif estado == 2 and millis_step <= millis():
            bus_1.write_i2c_block_data(xiao, 2, [1, 100])  # Avance
            move = True
            estado = 0
            millis_step = millis() + 50
        elif estado == 3 and millis_step <= millis():
            print('estado_3')
            # Giro 90 grados negativo
            cache.set('giro_start', True)
            nav_giro(90 + c_ang, estado)
            estado = 0
            move = True
            millis_step = millis() + 50
        elif estado == 4 and millis_step <= millis():
            print('estado_4')
            # Giro 90 grados positivo
            cache.set('giro_start', True)
            nav_giro(90 + c_ang, estado)
            estado = 0
            move = True
            millis_step = millis() + 50
        elif estado == 5 and millis_step <= millis():
            print('estado_5')
            # Retroceso
            bus_1.write_i2c_block_data(xiao, 2, [2, 100])  # Retroceso
            move = True
            estado = 0
            millis_step = millis() + 50'''
    bus_1.write_i2c_block_data(xiao, 2, [5, 0])  # Se detiene el robot
    bus_1.write_byte(teensy, 0)  # Se detienen los sensores US


def mov_direction(val, move):
    # Info de sensores: [frontales, traseros, lateral_izq, lateral_der] [derecho, izquierdo]
    val_ord = [(val[1], val[2]), (val[5], val[6]), (val[3], val[4]), (val[7], val[0])]
    c_ang = 0
    if move:
        check_all = list(filter(lambda x: x <= 12, val))
        if len(check_all) > 0: return 1, c_ang
        if val_ord[0][0] <= 14 or val_ord[0][1] <= 14: return 1, c_ang  # Detener robot
    if val_ord[0][0] > 14 and val_ord[0][1] > 14:
        return 2, c_ang  # Avanzar
    elif val_ord[1][0] > 12 and val_ord[1][1] > 12:
        if val_ord[1][0] != val_ord[1][1]: c_ang = int(math.atan((val_ord[1][1] - val_ord[1][0]) / 7) * 180 / math.pi)
        return 3, c_ang  # Giro izquierda 90 grados
    elif val_ord[2][0] > 12 and val_ord[2][1] > 12:
        if val_ord[2][0] != val_ord[2][1]: c_ang = int(math.atan((val_ord[2][1] - val_ord[2][0]) / 7) * 180 / math.pi)
        return 4, c_ang  # Giro derecha 90 grados
    elif val_ord[3][0] > 12 and val_ord[3][1] > 12:
        return 5, c_ang  # Retroceder
    # val = list(enumerate(val))
    # filtro_us = list(filter(lambda x: x[1] > 11, val))


def mov_direction_2(val, move):
    c_ang = 0
    sensor = Sensores(val)
    check_all = sensor.check_all()
    if len(check_all) > 0 and move:
        return 1, c_ang
    elif len(check_all) > 0 and not move:
        check_list = sensor.check_list()
        check_sum = sum(check_list)
        if check_sum > 6:
            return 1, c_ang
        elif check_sum == 6 and sensor.front_l > 15 and sensor.front_r > 15:
            if sensor.left_f > sensor.left_b and sensor.rear_f > sensor.rear_b:
                return 2, c_ang
        elif check_sum == 6 and sensor.back_l > 15 and sensor.back_r > 15:
            if sensor.left_f < sensor.left_b and sensor.rear_f < sensor.rear_b:
                return 2, c_ang
        # elif
        else:
            return 1, c_ang
    else:
        val_ord = [(val[1], val[2]), (val[5], val[6]), (val[3], val[4]), (val[7], val[0])]
        if val_ord[2][0] <= 14 and val_ord[2][0] < val_ord[2][1]:
            c_ang = adj_ang_giro(val_ord[2][0], val_ord[2][1])
        if val_ord[3][1] <= 14 and val_ord[3][1] < val_ord[3][0]:
            c_ang = adj_ang_giro(val_ord[3][0], val_ord[3][1])


def adj_ang_giro(val_1, val_2):
    return int(math.atan((val_1 - val_2) / 7) * 180 / math.pi)


def nav_giro(ang, dir_giro):  # Instrucción de giro del Robot
    estado_giro = 0
    millis_ini = 0
    millis_imu = 0
    imu_array = []
    while cache.get('giro_start'):
        if estado_giro == 0:
            data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
            data_imu = leer_euler(data_imu)
            yaw_ini = data_imu[0]
            yaw_final = yaw_ini + 45
            imu_array = get_imu_array_r(data_imu[0], ang, dir_giro)
            # if yaw_final > 360: yaw_final = yaw_final - 360
            # print(imu_array)
            estado_giro = 2
        elif estado_giro == 2:
            bus_1.write_i2c_block_data(xiao, 2, [dir_giro, 100])
            estado_giro = 3
        elif estado_giro == 3 and millis() >= millis_imu:
            data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
            data_imu = leer_euler(data_imu)[0]
            if round(data_imu) == 360: data_imu = 0
            if round(data_imu) in imu_array:
                val_check = len(imu_array) - imu_array.index(round(data_imu)) <= 12
            else:
                val_check = True
            # if round(data_imu) not in imu_array or val_check:
            if val_check:
                bus_1.write_i2c_block_data(xiao, 2, [5, 100])  # Detener robot
                estado_giro = 4
            else:
                millis_ini = millis() + 500
            millis_imu = millis() + 10
        elif estado_giro == 4:
            if millis_ini <= millis():
                data_imu = bus_2.read_i2c_block_data(f_imu, 0x1A, 6)
                data_imu = leer_euler(data_imu)[0]
                # print(int(data_imu), imu_array[-1])
                if int(data_imu) not in imu_array:
                    if dir_giro == 3:
                        dir_rev = 4  # Dirección opuesta de giro
                    else:
                        dir_rev = 3
                    bus_1.write_i2c_block_data(xiao, 4, [1, 1, 20, dir_rev])
                    millis_ini = millis() + 200
                elif int(data_imu) != imu_array[-1]:
                    bus_1.write_i2c_block_data(xiao, 4, [1, 1, 20, dir_giro])
                    millis_ini = millis() + 200
                else:
                    # print(data_imu, 'Final mov')
                    # estado_giro = 0
                    # inicio = True
                    cache.set('giro_start', False)
                    break


def set_delay(inicio):
    if inicio:
        inicio = False
        millis_step = millis()
    else:
        millis_step = millis() + 500
    return millis_step, inicio


def get_imu_array_r(ang_ini, delta, dir_giro):  # dir_giro >> 3 para giro izquierda; 4 para giro derecha
    ang_ini = round(ang_ini)
    salida = []
    for i in range(0, delta + 1):
        if dir_giro == 3:
            if (ang_ini - i) >= 0:
                salida.append(ang_ini - i)
            else:
                salida.append(360 + ang_ini - i)
        elif dir_giro == 4:
            if (i + ang_ini) <= 359:
                salida.append(i + ang_ini)
            else:
                salida.append(i + ang_ini - 360)
    # print(type(salida[0]))
    return salida


def millis():
    return round(time.time() * 1000)


# Test de listas
# import random
#
#
# def rand():
#     return random.randint(5, 250), random.randint(5, 250)
#
#
# lista_1 = [0] * 8
# for i in range(0, 5):
#     # print(i)
#     rand_l = list(rand() + rand() + rand() + rand())
#     lista_1 = list(zip(lista_1, rand_l))
#     lista_1 = list(map(lambda x: x[0] + x[1], lista_1))
# print(lista_1)
# lista_2 = list(map(lambda x: x / 5, lista_1))
# print(lista_2)
# sensor = Sensores(lista_2)
# print(sensor.rear_f)
