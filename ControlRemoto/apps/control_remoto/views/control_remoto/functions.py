import time

import sys
from collections import Counter

from smbus2 import SMBus

# import smbus


# ***************Funciones_IMU****************
from apps.control_remoto.models import InfoTest, SelectParametros


def leer_euler(data):
    result = [0, 0, 0]
    for i in range(3):
        result[i] = (((data[(i * 2) + 1] & 0xFF) << 8) | (data[(i * 2)] & 0xFF)) & 0xFFFF
        if result[i] & 0x8000:  # > 32767:
            result[i] -= 0x10000  # 65536
    result = [result[0] / 16, result[1] / 16, result[2] / 16]
    return result


def leer_cuaternion(data):
    result = [0, 0, 0, 0]
    for i in range(4):
        result[i] = (((data[(i * 2) + 1] & 0xFF) << 8) | (data[(i * 2)] & 0xFF)) & 0xFFFF
        if result[i] & 0x8000:  # > 32767:
            result[i] -= 0x10000  # 65536
    factor = (1.0 / (1 << 14))
    result = [result[0] * factor, result[1] * factor, result[2] * factor, result[3] * factor]
    return result


# *********************************************


# *************PWM**************


# if len(sys.argv) != 4:
#     print("usage:", sys.argv[0], "<tiempo> <direccion> <pasos>")
#     sys.exit(1)
#
# tiempo, direccion, pasos = sys.argv[1:4]

def data_sensores():
    # ***********Configuración i2c***************
    bus = SMBus(0)
    teensy = 0x8
    xiao = 0xA
    f_imu = 0x28

    # ***********fusion_IMU**********************
    bus.write_byte_data(f_imu, 0x3d, 0)
    time.sleep(0.1)
    # time.sleep(1)

    bus.write_byte_data(f_imu, 0x3f, 128)
    # time.sleep(0.2)
    time.sleep(0.8)

    bus.write_byte_data(f_imu, 0x3d, 12)
    time.sleep(0.1)

    data = bus.read_i2c_block_data(f_imu, 0x1A, 14)
    l_euler = data[:6]
    l_cuaternion = data[6:]
    e_v = leer_euler(l_euler)
    q_v = leer_cuaternion(l_cuaternion)

    block_1 = [e_v[0], e_v[1], e_v[2], q_v[0], q_v[1], q_v[2], q_v[3]]

    # *********Teensy**************
    block_2 = bus.read_i2c_block_data(teensy, 0, 8)
    block_2 += block_1

    return block_2


def control_r(direccion, pasos):
    bus = SMBus(0)
    # teensy = 0x8
    xiao = 0xA

    pwm = 130
    # data_1 = []

    tiempo = 100

    data_1 = [tiempo, direccion, pasos, pwm, 0]
    data_2 = bytearray(data_1)

    bus.write_i2c_block_data(xiao, 0x00, data_2)


def lista_test_dt(mod):
    check = 0
    if mod.ma_kp: check += 1
    if mod.ma_ki: check += 1
    if mod.ma_kd: check += 1
    if mod.mb_kp: check += 1
    if mod.mb_ki: check += 1
    if mod.mb_kd: check += 1
    num_test = str(mod.id) + '/' + str(check)
    salida = {
        'num_test': num_test,
        'tipo_prueba': mod.tipo_test,
        'time_test': mod.time_test,
        'fecha': mod.creado_el.date()
    }
    return salida


def gen_info_test_bak(modelo):
    velocidad = round(1000 / float(modelo.vel_rueda), 4)
    if modelo.dir_giro:
        dir_giro = 1
    else:
        dir_giro = 0
    salida = {
        'n_pulso': modelo.n_pulso,
        'tiempo': modelo.vel_rueda,
        'velocidad': str(velocidad),
        'dir_giro': dir_giro
    }
    return salida


def gen_info_test(modelo):
    velocidad = round(1000 / float(modelo.vel_rueda), 4)
    if modelo.dir_giro:
        dir_giro = 1
    else:
        dir_giro = 0
    salida = [
        modelo.n_pulso,
        modelo.vel_rueda,
        str(velocidad),
        dir_giro
    ]
    return salida


def ajuste_retraso(lista):
    salida = [[0, 0, 0, lista[0][3]], [1, 10, 0, lista[0][3]]]
    suma = 0
    for i in lista:
        suma += i[1]
        if i[0] == 1:
            salida.append([i[0] + 1, suma, 1000/(i[1] - 10), i[3]])
        else:
            salida.append([i[0] + 1, suma, i[2], i[3]])
    return salida


def info_global_test(modelo):
    modelo = modelo[0].test_rel
    salida = {
        'tipo_test': modelo.tipo_test,
        'time_test': modelo.time_test,
        'lvl_pwm': modelo.lvl_pwm
    }
    return salida


def def_ft(modelo):
    t_delta = [0] + list(map(lambda x: x.vel_rueda / 1000, modelo))
    k_delta = [0] + list(map(lambda x: round(1000 / x.vel_rueda, 4), modelo))
    # k_delta = k_delta
    # common = Counter(k_delta).most_common()[0][0]
    common = Counter(k_delta).most_common()
    # print(common[0][0], common[0][1], 'es el mas común')
    # print(common[1][0], common[1][1], 'es el 2 mas común')
    kdu = (common[0][0] * common[0][1] + common[1][0]*common[1][1]) / (common[0][1] + common[1][1])
    # print(kdu, 'es el kdu')
    # Cálculo de Tau
    val_1 = 632 * common[0][0] / 1000
    # Tiempo transcurrido
    sumador = 0
    t_tr = []
    for i in t_delta:
        sumador += i
        t_tr.append(sumador)
    # print(len(k_delta), len(t_tr), 'longitud de arrays')
    # print(k_delta)
    # print(val_1, 'previa de tau')
    tau = 0
    code_func = ''
    for i in range(0, len(k_delta)):
        if val_1 == k_delta[i + 1]:
            tau = round(t_tr[i + 1], 4)
            break
        elif val_1 < k_delta[i + 1]:
            # print(k_delta[i + 1], k_delta[i], 'valores mayor y menor')
            # print(t_tr, 'valores de ttr')
            delta = k_delta[i + 1] - k_delta[i]
            delta = (val_1 - k_delta[i]) / delta
            tau = round(t_tr[i] + (t_tr[i + 1] - t_tr[i]) * delta, 4)
            code_func = '[kp, ki, kd] = f_k_pid(' + str(common[0][0]) + ', ' + str(tau) + ')'
            break
    return {'kdu': round(kdu, 4), 'tau': tau, 'code_func': code_func}


def check_k_pid_post(post):
    print(post)
    salida = True
    if not post['kp_val'].replace(".", "", 1).isdigit():
        salida = False
    if not post['ki_val'].replace(".", "", 1).isdigit():
        salida = False
    if not post['kd_val'].replace(".", "", 1).isdigit():
        salida = False
    if salida:
        id_test, p_rueda = post['info_test'].split(',')
        objeto = InfoTest.objects.get(id=id_test)
        if p_rueda == '1':
            objeto.ma_kp = post['kp_val']
            objeto.ma_ki = post['ki_val']
            objeto.ma_kd = post['kd_val']
        elif p_rueda == '2':
            objeto.mb_kp = post['kp_val']
            objeto.mb_ki = post['ki_val']
            objeto.mb_kd = post['kd_val']
        objeto.save()
    return salida


def lista_ajustes_disp():
    consulta_1 = InfoTest.objects.filter(
        ma_kp__isnull=False,  # ma_kp='',
        ma_ki__isnull=False,  # ma_ki='',
        ma_kd__isnull=False,  # ma_kd='',
        mb_kp__isnull=False,  # mb_kp='',
        mb_ki__isnull=False,  # mb_ki='',
        mb_kd__isnull=False  # mb_kd=''
    )
    avance = list(map(map_lista_ajustes, filter(lambda x: x.tipo_test == 1, consulta_1)))
    retroceso = list(map(map_lista_ajustes, filter(lambda x: x.tipo_test == 2, consulta_1)))
    giro_izq = list(map(map_lista_ajustes, filter(lambda x: x.tipo_test == 3, consulta_1)))
    giro_der = list(map(map_lista_ajustes, filter(lambda x: x.tipo_test == 4, consulta_1)))
    select_info = active_pr()
    print(select_info)
    salida = {
        'avance': avance,
        'retroceso': retroceso,
        'giro_izq': giro_izq,
        'giro_der': giro_der,
        'val_select': active_pr()
    }
    return salida


def active_pr():
    query_1 = SelectParametros.objects.filter(activo=True)
    if query_1:
        salida = [
            query_1[0].avance_id,
            query_1[0].retroceso_id,
            query_1[0].giro_izq_id,
            query_1[0].giro_der_id,
        ]
    else:
        salida = []
    return salida


def map_lista_ajustes(model):
    if model is not []:
        salida = [
            model.id, model.lvl_pwm, model.time_test
        ]
    else:
        salida = None
    return salida


def form_k_rueda(mod):
    info_r = mod[0].test_rel
    rueda = mod[0].pos_rueda
    if rueda == 1:
        if info_r.ma_kp and info_r.ma_ki and info_r.ma_kd:
            salida = [
                info_r.ma_kp,
                info_r.ma_ki,
                info_r.ma_kd,
            ]
        else:
            salida = []
    else:
        if info_r.mb_kp and info_r.mb_ki and info_r.mb_kd:
            salida = [
                info_r.mb_kp,
                info_r.mb_ki,
                info_r.mb_kd,
            ]
        else:
            salida = []
    return salida


def save_global_pr(post):
    query_1 = SelectParametros.objects.filter(id=1)
    if query_1:
        query_1[0].avance_id = post['ks_avance']
        query_1[0].retroceso_id = post['ks_retroceso']
        query_1[0].giro_izq_id = post['ks_giro_izq']
        query_1[0].giro_der_id = post['ks_giro_der']
        query_1[0].activo = True
        query_1[0].save()
    else:
        mod = SelectParametros()
        mod.avance_id = post['ks_avance']
        mod.retroceso_id = post['ks_retroceso']
        mod.giro_izq_id = post['ks_giro_izq']
        mod.giro_der_id = post['ks_giro_der']
        mod.activo = True
        mod.save()
