from time import sleep


def check_i2c(bus_1, xiao, tipo, lvl_pwm, tiempo, val=0):
    salida_1 = []
    salida_2 = []
    tiempo = tiempo.to_bytes(2, 'big')
    # Arreglo con instrucciones: Tipo test | Nivel PWM | Tiempo test
    data = [tipo, lvl_pwm, tiempo[0], tiempo[1]]
    flag = 1
    st_flag = 1
    while flag == 1:
        if st_flag == 1:
            bus_1.write_i2c_block_data(xiao, val, data)
            st_flag = 0
        else:
            # val = 1
            bus_1.write_byte(xiao, val)
        xiao_data = bus_1.read_byte(xiao)
        if xiao_data == 1:
            xiao_data = bus_1.read_i2c_block_data(xiao, 0, 5)
            salida_1.append([xiao_data[0] << 8 | xiao_data[1], xiao_data[2] << 8 | xiao_data[3]])
        elif xiao_data == 2:
            xiao_data = bus_1.read_i2c_block_data(xiao, 0, 5)
            salida_2.append([xiao_data[0] << 8 | xiao_data[1], xiao_data[2] << 8 | xiao_data[3]])
        elif xiao_data == 3:
            xiao_data = bus_1.read_i2c_block_data(xiao, 0, 10)
            salida_1.append([xiao_data[0] << 8 | xiao_data[1], xiao_data[2] << 8 | xiao_data[3]])
            salida_2.append([xiao_data[5] << 8 | xiao_data[6], xiao_data[7] << 8 | xiao_data[8]])
        elif xiao_data == 4:
            print(xiao_data)
            flag = 0
        sleep(0.0010)
    print(salida_1)
    print(salida_2)
    return salida_1, salida_2


class Sensores:
    def __init__(self, lista):
        self.sensores = lista
        self.front_l = lista[2]
        self.front_r = lista[1]
        self.back_l = lista[6]
        self.back_r = lista[5]
        self.left_f = lista[3]
        self.left_b = lista[4]
        self.rear_f = lista[7]
        self.rear_b = lista[0]

    def check_all(self):
        return list(filter(lambda x: x <= 13, self.sensores))

    def check_list(self):
        def check_sensor(val):
            if val <= 12:
                return 1
            else:
                return 0
        check_list = list(map(check_sensor, self.sensores))
        return check_list

    def dir_info(self):
        lista_1 = (
            sum([self.front_l, self.front_r]) / 2,  # 0 Frente
            sum([self.front_l, self.left_f]) / 2,  # 1 Esquina frontal izquierda
            sum([self.left_f, self.left_b]) / 2,  # 2 Lateral izquierdo
            sum([self.left_b, self.back_r]) / 2,  # 3 Esquina trasera derecha
            sum([self.back_r, self.back_l]) / 2,  # 4 Trasera
            sum([self.back_l, self.rear_b]) / 2,  # 5 Esquina trasera izquierda
            sum([self.rear_b, self.rear_f]) / 2,  # 6 Lateral derecho
            sum([self.rear_f, self.front_r]) / 2,  # 7 Esquina frontal derecha
        )
        lista_2 = list(lista_1)
        lista_2.sort(reverse=True)
        lista_3 = list(map(lambda x: lista_1.index(x), lista_2))
        if len(set(lista_3)) != 8:
            check_list = [0, 1, 2, 3, 4, 5, 6, 7]
            check = list(filter(lambda x: x not in lista_3, check_list))
            if len(check) > 0:
                duplicado = list(filter(lambda x: lista_3.count(x) > 1, check_list))
                index_val = lista_3.index(duplicado[0])
                lista_3[index_val] = check[0]
        return lista_3

    def print_all(self):
        s = self
        print(s.front_l, s.front_r, s.left_f, s.rear_f, s.left_b, s.rear_b, s.back_l, s.back_r)
