let info_sensores = document.getElementById('info_sensores');
let giro_45 = document.getElementById('giro_45');
let giro_90 = document.getElementById('giro_90');
let giro_180 = document.getElementById('giro_180');
let giro_270 = document.getElementById('giro_270');
let giro_360 = document.getElementById('giro_360');
let stop_while = document.getElementById('stop_while');
let nav_start = document.getElementById('nav_robot');

info_sensores.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'info_sensores'));
giro_45.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'giro_45'));
giro_90.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'giro_90'));
giro_180.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'giro_180'));
giro_270.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'giro_270'));
giro_360.addEventListener('click', nav_robot.bind(null, 'mov_robot', 'giro_360'));
stop_while.addEventListener('click', nav_robot.bind(null, 'stop_loop', 'stop_while'));
nav_start.addEventListener('click', nav_robot.bind(null, 'nav_robot', ''));

async function nav_robot(action, info){
    let parameters = new FormData();
    parameters.append('info', info);
    let data = await global_fetch(action, parameters);
    console.log(data);
}