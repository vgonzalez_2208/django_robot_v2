// var csrftoken;

let tabla_test = $('#tabla_lista_pid').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    "searching": false,
    "lengthChange": false,
    // "bPaginate": false,
    "bInfo": false,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'lista_test'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "num_test"},
        {"data": "tipo_prueba"},
        {"data": "time_test"},
        {"data": "fecha"},
        {"data": "num_test"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            render: function (data, type, row) {
                let val = data.split('/');
                let salida = '<button class="btn btn-primary btn-sm" onclick="ver_test(' + val[0] + ')"><i class="fa-solid fa-eye"></i></i></button>';
                salida += '&nbsp<button class="btn btn-danger btn-sm" onclick="borrar_test(' + val[0] + ')"><i class="fa-solid fa-trash-can"></i></button>';
                return salida
            },
        },
        {
            targets: [-2],
            class: 'text-center',
        },
        {
            targets: [-3],
            class: 'text-left',
            render: function (data, type, row) {
                return data + ' ms.';
            },
        },
        {
            targets: [-4],
            class: 'text-left',
            render: function (data, type, row) {
                if (data === 1) {
                    return '<i class="fa-solid fa-lg fa-angles-up"></i> Avance';
                } else if (data === 2) {
                    return '<i class="fa-solid fa-lg fa-angles-down"></i> Retroceso';
                } else if (data === 3) {
                    return '<i class="fa-solid fa-lg fa-angles-left"></i> Giro';
                } else if (data === 4) {
                    return '<i class="fa-solid fa-lg fa-angles-right"></i> Giro';
                } else {
                    return '<span class="badge badge-pill badge-warning">Visto</span>';
                }
            },
        },
        {
            targets: [-5],
            class: 'text-left',
            render: function (data, type, row) {
                let val = data.split('/');
                if (val[1] === '6') {
                    return '<span class="badge bg-success">' + val[0] + '</span>';
                } else if (val[1] === '0') {
                    return '<span class="badge bg-secondary">' + val[0] + '</span>';
                } else {
                    return '<span class="badge bg-warning text-dark">' + val[0] + '</span>';
                }
            },
        },
    ],
    initComplete: function (settings, json) {
    }
});

let btn_test = document.getElementById('btn_test');
let hostname_url = window.location.hostname;
let socket = new WebSocket('ws://' + hostname_url + ':8000/ws/canal_fd/');

btn_test.onclick = function () {
    btn_click('form_test', new FormData(document.getElementById('form_test')));
}

function btn_click(tipo, form) {
    let salida = {};
    let error_list = [];
    if (form.get('msg') === "") {
        error_list.push('Seleccione el tipo de test.');
    }
    if (form.get('select_pwm') === "") {
        error_list.push('Seleccione un nivel de PWM.');
    }
    if (form.get('tiempo_captura') === "") {
        error_list.push('Ingrese un tiempo de captura en ms.');
    }
    if (isNaN(form.get('tiempo_captura'))) {
        error_list.push('Ingrese un número válido.');
    }
    if (error_list.length > 0) {
        let errors = '<ul class="text-start h5">';
        error_list.forEach(function (val) {
            errors += '<li>' + val + '</li>';
        });
        errors += '</ul>'
        Swal.fire({
            html: errors,
        });
    } else {
        form.forEach((value, key) => salida[key] = value);
        console.log(salida);
        socket.send(JSON.stringify(salida));
    }
}

socket.onmessage = function (event) {
    let data = JSON.parse(event.data);
    console.log(data);
    if ('errors' in data) {
        alert(data['errors'].length);
    }
    let info = data['test'];
    if (info === 'Completado') {
        save_alert('Completado', 'success', 1000);
    } else if (info === 'Error') {
        save_alert('Error', 'warning', 1000);
    }
    tabla_test.ajax.reload()
}

// Funciones de la tabla de tests
async function ver_test(data) {
    Swal.fire({
        title: 'Seleccione posición de la rueda:',
        showDenyButton: true,
        // showCancelButton: true,
        confirmButtonText: 'Izquierda ' + '<i class="fa-solid fa-lg fa-angles-left">',
        denyButtonText: 'Derecha' + ' <i class="fa-solid fa-lg fa-angles-right">',
    }).then((result) => {
        let action_info_test = 'ver_test';
        // let id_test = data;
        if (result.isConfirmed) {
            let pos_rueda = 1;
            gen_tabla_info(action_info_test, data, pos_rueda);
            document.getElementById('last_test').value = [data, pos_rueda];
            let modal = new bootstrap.Modal(document.getElementById("modal_test_info"), {});
            modal.show();
        } else if (result.isDenied) {
            let pos_rueda = 2;
            gen_tabla_info(action_info_test, data, pos_rueda);
            document.getElementById('last_test').value = [data, pos_rueda];
            let modal = new bootstrap.Modal(document.getElementById("modal_test_info"), {});
            modal.show();
        }
    })
}

async function borrar_test(data, flg = true) {
    let msg;
    if (flg) {
        msg = '¿Eliminar el test?';
    } else {
        msg = '¿Eliminar todos los tests?';
    }
    Swal.fire({
        title: msg,
        showDenyButton: true,
        confirmButtonText: 'Sí',
        denyButtonText: 'No',
        denyButtonColor: 'blue',
        confirmButtonColor: 'red',
    }).then((result) => {
        if (flg) {
            if (result.isConfirmed) {
                (async () => {
                    let parameters = new FormData();
                    parameters.append('id_test', data);
                    let info = await global_fetch('borrar_test', parameters);
                    if (info['status'] === 'ok') {
                        save_alert('Eliminado', 'info', 1200);
                        tabla_test.ajax.reload();
                        await lista_ajustes_disponibles();
                    }
                })();
            }
        } else {
            if (result.isConfirmed) {
                (async () => {
                    let info = await global_fetch('borrar_todo_test');
                    if (info['status'] === 'ok') {
                        save_alert('Eliminados', 'info', 1200);
                        tabla_test.ajax.reload();
                        await lista_ajustes_disponibles();
                    }
                })();
            }
        }
    })
}

async function gen_tabla_info(action_info_test, id_test, pos_rueda) {
    let parameters = new FormData();
    parameters.append('id_test', id_test);
    parameters.append('pos_rueda', pos_rueda);
    let data = await global_fetch(action_info_test, parameters);
    // console.log(data);
    $('#tabla_info_test').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        "searching": false,
        "lengthChange": false,
        // "bPaginate": false,
        "bInfo": false,
        pageLength: -1,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        data: data[0],
        columns: [
            {title: "#"},
            {title: "Tiempo (ms)"},
            {title: "Velocidad(pulsos/s)"},
            {title: "Dirección"},
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<span class="h4">+ </span>' + '<i class="fa-solid fa-lg fa-angles-up"></i>';
                    } else {
                        return '<span class="h4">- </span>' + '<i class="fa-solid fa-lg fa-angles-down"></i>';
                    }
                },
            },
            {
                targets: [-2],
                class: 'text-center',
            },
            {
                targets: [-3],
                class: 'text-center',
                // render: function (data, type, row) {
                //     return data + ' ms.';
                // },
            },
            {
                targets: [-4],
                class: 'text-left',
            },
        ],
        initComplete: function () {
            // En index[0] se encuentra la info de la tabla, en index[1] la info complementaria del modal
            // En index[2] se encuentra info sobre el form de kp, ki y kd ligada a la rueda del test
            let info = data[1];
            let title_1 = document.getElementById('modal_title_1');
            let title_2 = document.getElementById('modal_title_2');
            let title_3 = document.getElementById('modal_title_3');
            let kdu = document.getElementById('kdu');
            let tau = document.getElementById('tau_val');
            let code_func = document.getElementById('code_func');
            let pos_rueda_span = '';
            if (info['tipo_test'] === 1) {
                title_1.innerText = 'Avance | ';
            } else if (info['tipo_test'] === 2) {
                title_1.innerText = 'Retroceso | ';
            } else if (info['tipo_test'] === 3) {
                title_1.innerText = 'Giro izquierda | ';
            } else if (info['tipo_test'] === 4) {
                title_1.innerText = 'Giro derecha | ';
            }
            title_2.innerText = 'Duración test: ' + info['time_test'] + ' ms. | ';
            title_3.innerText = 'Nivel PWM: ' + info['lvl_pwm'] + ' % | ';
            if (pos_rueda === 1) {
                pos_rueda_span = '<i class="fa-xl fa-regular fa-circle-left"></i>';
            } else {
                pos_rueda_span = '<i class="fa-xl fa-regular fa-circle-right"></i>';
            }
            document.getElementById('pos_rueda_span').innerHTML = pos_rueda_span;
            kdu.innerHTML = info['kdu'];
            tau.innerHTML = info['tau'];
            code_func.innerHTML = info['code_func'];
            info_k_rueda(data[2]);
        }
    });
}

function info_k_rueda(info) {
    // console.log(info.length);
    if (info.length > 0) {
        let form = document.forms['form_k_pid'];
        form['kp_val'].value = info[0];
        form['ki_val'].value = info[1];
        form['kd_val'].value = info[2];
    } else {
        document.getElementById('form_k_pid').reset();
    }
}

let k_pid_btn = document.getElementById('k_pid_btn');

k_pid_btn.onclick = async function () {
    let kp_val = document.forms['form_k_pid']['kp_val'].value;
    let ki_val = document.forms['form_k_pid']['ki_val'].value;
    let kd_val = document.forms['form_k_pid']['kd_val'].value;
    let errors = [];
    if (isNaN(kp_val) || kp_val === '') {
        errors.push('Ingrese un número válido.');
    }
    if (isNaN(ki_val) || ki_val === '') {
        errors.push('Ingrese un número válido.');
    }
    if (isNaN(kd_val) || kd_val === '') {
        errors.push('Ingrese un número válido.');
    }
    if (errors.length > 0) {
        error_form_msg(errors);
    } else {
        let parameters = new FormData(document.getElementById('form_k_pid'));
        let data = await global_fetch('k_pid', parameters);
        if (data['status'] === 'ok') {
            save_alert('Parámetros PID guardados', 'success', 1000);
            tabla_test.ajax.reload();
            await lista_ajustes_disponibles();
        } else if (data['status'] === 'error') {
            save_alert('Ocurrió un error', 'error', 1200);
        }
    }
}


async function lista_ajustes_disponibles() {
    let data = await global_fetch('select_pr');
    if (data['status'] === 'ok') {
        select_ajustes('ks_avance', data['avance'], data['val_select'][0]);
        select_ajustes('ks_retroceso', data['retroceso'], data['val_select'][1]);
        select_ajustes('ks_giro_izq', data['giro_izq'], data['val_select'][2]);
        select_ajustes('ks_giro_der', data['giro_der'], data['val_select'][3]);
    }
}


function select_ajustes(id_select, info, val_select) {
    // console.log(val_select);
    let select = document.getElementById(id_select);
    let options = '<option value="">----Seleccione----</option>';
    if (info.length > 0) {
        info.forEach(function (val) {
            let msg = msg_select_ajustes(val);
            if (val[0] === val_select) {
                options += '<option selected value="' + val[0] + '">' + msg + '</option>';
            } else {
                options += '<option value="' + val[0] + '">' + msg + '</option>';
            }
        });
        select.innerHTML = options;
    } else {
        select.innerHTML = '<option value="">-------------------</option>';
    }
}


function msg_select_ajustes(val) {
    let msg = '';
    msg += 'Test #' + val[0] + ' | ';
    msg += '% PWM: ' + val[1] + ' | ';
    msg += 'Duración: ' + val[2] + ' ms.';
    return msg
}

let btn_global_pr = document.getElementById('btn_global_pr');

btn_global_pr.onclick = async function () {
    let form = document.forms['form_global_pr'];
    let errors = [];
    if (form['ks_avance'].value === '') {
        errors.push('Seleccione parámetros de avance.');
    }
    if (form['ks_retroceso'].value === '') {
        errors.push('Seleccione parámetros de retroceso.');
    }
    if (form['ks_giro_izq'].value === '') {
        errors.push('Seleccione parámetros de giro izq.');
    }
    if (form['ks_giro_der'].value === '') {
        errors.push('Seleccione parámetros de giro der.');
    }
    if (errors.length > 0) {
        error_form_msg(errors);
    } else {
        let parameters = new FormData(document.getElementById('form_global_pr'));
        let data = await global_fetch('save_global_pr', parameters);
        if (data['status'] === 'ok') {
            save_alert('Parámetros guardados.', 'success', 1200);
        }
    }
}

function error_form_msg(errors) {
    let msg = '<ul class="text-start h5">';
    errors.forEach(function (val) {
        msg += '<li>' + val + '</li>';
    });
    msg += '</ul>'
    Swal.fire({
        html: msg,
    });
}

// function copiar_texto(id_value) {
//     let copy_text = document.getElementById(id_value);
//     let textArea = document.createElement("textarea");
//     textArea.value = copy_text.textContent;
//     document.body.appendChild(textArea);
//     textArea.select();
//     // navigator.clipboard.writeText(textArea.value);
//     document.execCommand('copy');
//     textArea.remove();
// }