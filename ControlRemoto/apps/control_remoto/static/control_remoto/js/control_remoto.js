function direction_button(direction) {
    let url = window.location.pathname;
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'control_remoto');
    data.append('direction', direction);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                console.log(response);
                // alert(response);
                return false;
            } else {
                // message_error(response);
            }
        });
}

function data_sensores() {
    let url = window.location.pathname;
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'data_sensores');
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                document.getElementById('sensor_1').innerHTML = response['us_1'] + ' cm.';
                document.getElementById('sensor_2').innerHTML = response['us_2'] + ' cm.';
                document.getElementById('sensor_3').innerHTML = response['us_3'] + ' cm.';
                document.getElementById('sensor_4').innerHTML = response['us_4'] + ' cm.';
                document.getElementById('sensor_5').innerHTML = response['us_5'] + ' cm.';
                document.getElementById('sensor_6').innerHTML = response['us_6'] + ' cm.';
                document.getElementById('sensor_7').innerHTML = response['us_7'] + ' cm.';
                document.getElementById('sensor_8').innerHTML = response['us_8'] + ' cm.';
                // alert(response);
                return false;
            } else {
                // message_error(response);
            }
        });
}

// setInterval(data_sensores, 1000);

let btn_start = document.getElementById('btn_start');
let btn_stop = document.getElementById('btn_stop');
let btn_check = document.getElementById('btn_check');
let hostname_url = window.location.hostname;
let socket = new WebSocket('ws://' + hostname_url + ':8000/ws/canal_fd/');

btn_start.onclick = async function () {
    // alert('btn start');
    // var socket = new WebSocket('ws://192.168.0.172:8000/ws/canal_fd/');
    console.log(socket.readyState);
    if (socket.readyState === 1) {
        let mensaje = JSON.stringify({'msg': 'info_sensores'});
        socket.send(mensaje);
    } else if (socket.readyState === 3) {
        socket = new WebSocket('ws://' + hostname_url + ':8000/ws/canal_fd/');
        reinicio();
        console.log(socket.readyState);
        // // socket = new WebSocket('ws://192.168.0.172:8000/ws/canal_fd/');
        // let mensaje = JSON.stringify({'msg': 'info_sensores'});
        // if (socket.readyState === 1) {
        //     await socket.send(mensaje);
        // }
    }
}

btn_stop.onclick = function () {
    if (socket.readyState === 1) {
        let mensaje = JSON.stringify({'msg': 'close'});
        socket.send(mensaje);
    }
}

socket.onmessage = function (event) {
    let data = JSON.parse(event.data);
    console.log(data);
    document.getElementById('sensor_1').innerHTML = data['us_1'] + ' cm.';
    document.getElementById('sensor_2').innerHTML = data['us_2'] + ' cm.';
    document.getElementById('sensor_3').innerHTML = data['us_3'] + ' cm.';
    document.getElementById('sensor_4').innerHTML = data['us_4'] + ' cm.';
    document.getElementById('sensor_5').innerHTML = data['us_5'] + ' cm.';
    document.getElementById('sensor_6').innerHTML = data['us_6'] + ' cm.';
    document.getElementById('sensor_7').innerHTML = data['us_7'] + ' cm.';
    document.getElementById('sensor_8').innerHTML = data['us_8'] + ' cm.';
}

function reinicio() {
    if (socket.readyState !== 1) {
        setTimeout(reinicio, 50);//wait 50 millisecnds then recheck
    } else {
        console.log('se inicio', socket.readyState);
        let mensaje = JSON.stringify({'msg': 'info_sensores'});
        socket.send(mensaje);
        socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            console.log(data);
        }
    }
}

btn_check.onclick = function () {
    if (socket.readyState === 1) {
        let mensaje = JSON.stringify({'msg': 'check_cache'});
        socket.send(mensaje);
    }
}

// Botones de movimiento
let btn_up = document.getElementById('btn_up');
let btn_down = document.getElementById('btn_down');
let btn_left = document.getElementById('btn_left');
let btn_right = document.getElementById('btn_right');
let btn_stop_rb = document.getElementById('btn_stop_rb');
let btn_reload_rb = document.getElementById('btn_reload_rb');

btn_up.addEventListener('click', send_msg_ws.bind(null, 'mov_robot', 'btn_up'));
btn_down.addEventListener('click', send_msg_ws.bind(null, 'mov_robot', 'btn_down'));
btn_left.addEventListener('click', send_msg_ws.bind(null, 'mov_robot', 'btn_left'));
btn_right.addEventListener('click', send_msg_ws.bind(null, 'mov_robot', 'btn_right'));
btn_stop_rb.addEventListener('click', send_msg_ws.bind(null, 'mov_robot', 'btn_stop_rb'));
btn_reload_rb.addEventListener('click', send_msg_ws.bind(null, 'stop_mov_robot', 'btn_reload_rb'));

function send_msg_ws(msg, dir = null) {
    if (socket.readyState === 1) {
        let mensaje = JSON.stringify({'msg': msg, 'dir': dir});
        socket.send(mensaje);
    }
}