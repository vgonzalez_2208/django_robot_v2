import threading

from channels.generic.websocket import WebsocketConsumer

from apps.control_remoto.ws_functions import *


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class WSConsumers(WebsocketConsumer):
    def connect(self):
        # To accept the connection call:
        self.accept()

    def receive(self, text_data=None, bytes_data=None):
        # do something with data
        # print(text_data)
        text_data = json.loads(text_data)
        msg = text_data['msg']
        # print(caches['val_1'])
        # print(cache.get('val_2'))
        if msg == 'info_sensores':
            cache.set('send_info', True)
            # send_async(cargar_info_sensores, (self,))
            send_async(data_sensores_2, (self,))
        elif msg == 'check_cache':
            print(cache.get('send_info'))
        elif msg == 'close':
            cache.set('send_info', False)
        elif msg in ('tst_avanzar', 'tst_retroceder', 'tst_giro_izq', 'tst_giro_der'):
            iniciar_test(self, text_data)
        elif msg == 'mov_robot':
            # print('mov_robot')
            if cache.get('mov_robot'):
                change_mov_robot(text_data)
            else:
                cache.set('mov_robot', True)
                send_async(mov_robot, (self, text_data,))
        elif msg == 'stop_mov_robot':
            cache.set('mov_robot', False)
            print('mov_robot False')
            # send_async(mov_robot, (self, text_data,))
