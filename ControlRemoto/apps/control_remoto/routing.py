from django.urls import re_path, path

from .consumers import WSConsumers

# websocket_urlpatterns = [
#     re_path(r'ws/chat/(?P<room_name>\w+)/$', consumers.ChatConsumers.as_asgi())
# ]

ws_urlpatterns = [
    path('ws/canal_fd/', WSConsumers.as_asgi())
]
