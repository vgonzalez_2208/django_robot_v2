import itertools
import json
import time
from time import sleep

from django.core.cache import cache
from smbus2 import SMBus, i2c_msg

from apps.control_remoto.models import InfoTest, AjusteRuedasPID, SelectParametros
from apps.control_remoto.views.control_remoto.functions import leer_euler, leer_cuaternion


def data_sensores_2(socket):
    # ***********Configuración i2c***************
    bus_1 = SMBus(0)
    bus_2 = SMBus(1)
    teensy = 0x8
    xiao = 0xA
    f_imu = 0x28

    # ***********fusion_IMU**********************
    bus_2.write_byte_data(f_imu, 0x3d, 0)
    time.sleep(0.1)
    # time.sleep(1)

    bus_2.write_byte_data(f_imu, 0x3f, 128)
    # time.sleep(0.2)
    time.sleep(0.8)

    bus_2.write_byte_data(f_imu, 0x3d, 12)
    time.sleep(0.1)

    # ***********Teensy_setup**********************
    bus_1.write_byte(teensy, 1)
    time.sleep(0.01)

    while cache.get('send_info'):
        data = bus_2.read_i2c_block_data(f_imu, 0x1A, 14)
        l_euler = data[:6]
        l_cuaternion = data[6:]
        e_v = leer_euler(l_euler)
        q_v = leer_cuaternion(l_cuaternion)

        block_1 = [e_v[0], e_v[1], e_v[2], q_v[0], q_v[1], q_v[2], q_v[3]]

        # *********Teensy**************
        block_2 = bus_1.read_i2c_block_data(teensy, 0, 8)
        block_2 += block_1

        data = {
            'us_1': block_2[2],
            'us_2': block_2[3],
            'us_3': block_2[4],
            'us_4': block_2[5],
            'us_5': block_2[1],  #
            'us_6': block_2[0],  #
            'us_7': block_2[7],
            'us_8': block_2[6],
        }
        socket.send(json.dumps(data))
        # print('info enviada', cache.get('send_info'))
        sleep(1)
    bus_1.write_byte(teensy, 0)
    # time.sleep(0.1)


def iniciar_test(socket, data):
    errors = []
    if not data.get('select_pwm'):
        errors.append('Seleccione un nivel de PWM.')
    if not data.get('tiempo_captura'):
        errors.append('Ingrese un tiempo de captura de datos.')
    if errors:
        socket.send(json.dumps({'errors': errors}))
    else:
        msg = data['msg']
        lvl_pwm = int(data['select_pwm'])
        tiempo = int(data['tiempo_captura'])
        # ajuste_pid(msg, data['select_pwm'], data['tiempo_captura'], socket)
        if msg == 'tst_avanzar':
            print(data)
            ajuste_pid(1, lvl_pwm, tiempo, socket)
            # socket.send(json.dumps({'flag': 'ok_avanzar'}))
        elif msg == 'tst_retroceder':
            print(data)
            ajuste_pid(2, lvl_pwm, tiempo, socket)
            # socket.send(json.dumps({'flag': 'ok_retroceso'}))
        elif msg == 'tst_giro_izq':
            print(data)
            ajuste_pid(3, lvl_pwm, tiempo, socket)
            # socket.send(json.dumps({'flag': 'ok_giro_izq'}))
        elif msg == 'tst_giro_der':
            print(data)
            ajuste_pid(4, lvl_pwm, tiempo, socket)
            # socket.send(json.dumps({'flag': 'ok_giro_der'}))


def ajuste_pid(tipo: int, lvl_pwm: int, tiempo: int, socket, val=0):
    bus = SMBus(0)
    xiao = 0xA
    # ************************************ #
    val_ini = 0
    modelo = InfoTest()
    modelo.tipo_test = tipo
    modelo.time_test = tiempo
    modelo.lvl_pwm = lvl_pwm
    modelo.save()
    # Giro de rueda izq.
    if tipo in (1, 4):
        giro_izq = True
    else:
        giro_izq = False
    # Giro rueda der.
    if tipo in (1, 3):
        giro_der = True
    else:
        giro_der = False
    # pos_rueda_izq = False
    # pos_rueda_der = True
    # ************************************ #
    salida_1 = []
    salida_2 = []
    tiempo = tiempo.to_bytes(2, 'big')
    # Arreglo con instrucciones: Tipo test | Nivel PWM | Tiempo test
    data = [tipo, lvl_pwm, tiempo[0], tiempo[1]]
    flag = 1
    st_flag = 1
    while flag == 1:
        if st_flag == 1:
            bus.write_i2c_block_data(xiao, val, data)
            # sleep(0.004)
            st_flag = 0
        else:
            # val = 1
            bus.write_byte(xiao, val)
            # sleep(0.004)
        xiao_data = bus.read_byte(xiao)
        # sleep(0.004)
        if xiao_data == 1:  # Info rueda izquierda
            xiao_data = bus.read_i2c_block_data(xiao, 0, 4)
            salida_1.append(gen_sub_modelo(xiao_data, giro_izq, tipo, modelo))
            # salida_1.append([xiao_data[0], xiao_data[1] << 8 | xiao_data[2]])
        elif xiao_data == 2:  # Info rueda derecha
            xiao_data = bus.read_i2c_block_data(xiao, 0, 4)
            salida_2.append(gen_sub_modelo(xiao_data, giro_der, tipo, modelo))
            # salida_2.append([xiao_data[0], xiao_data[1] << 8 | xiao_data[2]])
        elif xiao_data == 3:
            xiao_data = bus.read_i2c_block_data(xiao, 0, 8)
            salida_1.append(gen_sub_modelo(xiao_data[0:4], giro_izq, tipo, modelo))
            salida_2.append(gen_sub_modelo(xiao_data[4:8], giro_der, tipo, modelo))
            # salida_1.append([xiao_data[0], xiao_data[1] << 8 | xiao_data[2]])
            # salida_2.append([xiao_data[4], xiao_data[5] << 8 | xiao_data[6]])
        elif xiao_data == 4:
            # print(xiao_data)
            flag = 0
        # elif xiao_data == 0:
        # sleep(0.004)
        # print('xiao 0')
        sleep(0.004)
    if len(salida_1) > 0 and len(salida_2) > 0:
        # print(modelo.id, 'sasas')
        AjusteRuedasPID.objects.bulk_create(salida_1 + salida_2)
        modelo.completado = True
        modelo.save()
        data_salida = {'test': 'Completado'}
    else:
        # sleep(3)
        consulta = InfoTest.objects.get(id=str(modelo.id))
        consulta.delete()
        data_salida = {'test': 'Error'}
        # print(modelo.id)
    socket.send(json.dumps(data_salida))
    # print(salida_1)
    # print(salida_2)
    return salida_1, salida_2


def gen_sub_modelo(data, dir_giro, tipo_test, test_rel):
    sub_modelo = AjusteRuedasPID()
    sub_modelo.pos_rueda = data[3]
    sub_modelo.dir_giro = dir_giro
    sub_modelo.tipo_test = tipo_test
    sub_modelo.vel_rueda = data[0]
    sub_modelo.n_pulso = data[1] << 8 | data[2]
    sub_modelo.test_rel = test_rel
    return sub_modelo


def mov_robot(socket, data):
    print(data['dir'])
    query = SelectParametros.objects.filter(
        id=1, avance__isnull=False, retroceso__isnull=False, giro_izq__isnull=False, giro_der__isnull=False
    )
    # print(query[0].avance)
    if query:
        print('Parámetros Ok')
        info = get_info_bytes(query[0])
        # print(info)
        bus = SMBus(0)
        # Dirección i²c del xiao
        xiao = 0xA
        # bus.write_i2c_block_data(xiao, 3, info)
        msg = i2c_msg.write(xiao, info)
        bus.i2c_rdwr(msg)
        # print(len(info), 'len info')
        change_mov_robot(data)
        bus.write_byte(xiao, 3)
        time.sleep(2)
        while cache.get('mov_robot'):
            if cache.get('flag_set_dir'):
                if cache.get('dir_robot') == 'avance':
                    data_i2c = [1, cache.get('v_robot')]
                    print('Dir de avance')
                elif cache.get('dir_robot') == 'retroceso':
                    data_i2c = [2, cache.get('v_robot')]
                    print('Dir retroceso')
                elif cache.get('dir_robot') == 'giro_izq':
                    data_i2c = [3, cache.get('v_robot')]
                    print('Dir giro izq')
                elif cache.get('dir_robot') == 'giro_der':
                    data_i2c = [4, cache.get('v_robot')]
                    print('Dir giro der')
                # elif cache.get('dir_robot') == 'tune_pid':
                #     data_i2c = [4, cache.get('v_robot')]
                #     print('Dir giro der')
                else:
                    data_i2c = [5, 0]
                    print('Stop robot')
                # Se envía primer byte como flag de control remoto
                bus.write_i2c_block_data(xiao, 2, data_i2c)
                cache.set('flag_set_dir', False)
        data_i2c = [5, cache.get('v_robot')]
        bus.write_i2c_block_data(xiao, 2, data_i2c)
        cache.set('flag_set_dir', False)
    else:
        print('error parametros')
        salida = {'error': 'No hay parámetros PID activos.'}
        socket.send(json.dumps(salida))


def get_info_bytes(model):
    # model = query[0]
    # info = [model.avance, model.retroceso, model.giro_izq, model.giro_der]
    # TODO Se ajusta información de Ks iguales a avance temporalmente
    info = [model.avance, model.avance, model.avance, model.avance]
    info = list(map(extract_ks, info))
    # print(model.avance.ma_kp)
    # print(model.giro_der.mb_kd)
    # print(info)
    info = list(map(lambda x: list(map(map_bytes, x)), info))
    # print(info)
    info = list(itertools.chain(*info))
    # print(info)
    info = list(itertools.chain(*info))
    # print(info)
    info = list(itertools.chain(*info))
    # TODO | Se adjunta nivel fijo de PWM pendiente de ajuste
    info.append(100)
    return info


def extract_ks(info):
    salida_1 = [info.ma_kp, info.ma_ki, info.ma_kd]
    salida_2 = [info.mb_kp, info.mb_ki, info.mb_kd]
    return salida_1, salida_2


def map_bytes(info):
    return list(map(bytes_ks, info))


def bytes_ks(info):
    decimal_k = int((info % 1) * 10000).to_bytes(2, 'big')
    return int(info), decimal_k[0], decimal_k[1]


def change_mov_robot(data, v_robot=30):  # v_robot corresponde al tiempo entre pulsos
    if data['dir'] == 'btn_up':
        cache.set('dir_robot', 'avance')
        cache.set('v_robot', v_robot)
    elif data['dir'] == 'btn_down':
        cache.set('dir_robot', 'retroceso')
        cache.set('v_robot', v_robot)
    elif data['dir'] == 'btn_left':
        cache.set('dir_robot', 'giro_izq')
        cache.set('v_robot', v_robot)
    elif data['dir'] == 'btn_right':
        cache.set('dir_robot', 'giro_der')
        cache.set('v_robot', v_robot)
    elif data['dir'] == 'btn_stop_rb':
        cache.set('dir_robot', 'stop')
        cache.set('v_robot', v_robot)
    cache.set('flag_set_dir', True)
