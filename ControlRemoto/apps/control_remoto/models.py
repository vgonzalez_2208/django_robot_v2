from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.


class InfoTest(models.Model):
    creado_el = models.DateTimeField(auto_now_add=True)
    # Tipo de test = dirección de movimiento del test; avance:1 ; retroceso: 2; giro izq.: 3; giro der.: 4.
    tipo_test = models.IntegerField(_('tipo test'))
    completado = models.BooleanField(_('completado'), default=False)
    # Tiempo de prueba
    time_test = models.PositiveSmallIntegerField(_('time test'))
    # Nivel de PWM aplicado
    lvl_pwm = models.PositiveSmallIntegerField(_('lvl pwm'))
    # Agregar campo de parámetros obtenidos (tau, KDu, etc)
    ganancia_k = models.DecimalField(_('ganancia k'), max_digits=7, decimal_places=4, null=True, blank=True)
    tau = models.DecimalField(_('tau'), max_digits=7, decimal_places=4, null=True, blank=True)
    # Resultados PID_Tuner motor A (izquierdo)
    ma_kp = models.DecimalField(_('ma kp'), max_digits=8, decimal_places=5, null=True, blank=True)
    ma_ki = models.DecimalField(_('ma ki'), max_digits=8, decimal_places=5, null=True, blank=True)
    ma_kd = models.DecimalField(_('ma kd'), max_digits=8, decimal_places=5, null=True, blank=True)
    # Resultados PID_Tuner motor B (derecho)
    mb_kp = models.DecimalField(_('mb kp'), max_digits=8, decimal_places=5, null=True, blank=True)
    mb_ki = models.DecimalField(_('mb ki'), max_digits=8, decimal_places=5, null=True, blank=True)
    mb_kd = models.DecimalField(_('mb kd'), max_digits=8, decimal_places=5, null=True, blank=True)


class AjusteRuedasPID(models.Model):
    creado_el = models.DateTimeField(auto_now_add=True)
    # Posición de las ruedas en el robot = izquierda: 1; derecha: 2.
    pos_rueda = models.PositiveSmallIntegerField(_('pos rueda'))
    # Dirección de giro de las ruedas en el robot = atrás: 0/False; adelante: 1/True.
    dir_giro = models.BooleanField(_('dir giro'))
    # Dirección de movimiento robot = avance: 1; retroceso: 2; giro izq.: 3; giro der.: 4.
    tipo_test = models.IntegerField(_('tipo test'))
    # Velocidad rueda = tiempo entre pulsos(pulsos/segundo).
    vel_rueda = models.DecimalField(_('vel rueda'), max_digits=4, decimal_places=1)
    # Contador de pulsos
    n_pulso = models.IntegerField(_('num pulso'))
    # Test relacionado
    test_rel = models.ForeignKey(InfoTest, on_delete=models.CASCADE)


class SelectParametros(models.Model):
    # Parámetros de dirección de avance
    avance = models.ForeignKey(InfoTest, related_name='avance', on_delete=models.SET_NULL, null=True, blank=True)
    # Parámetros de dirección de retroceso
    retroceso = models.ForeignKey(InfoTest, related_name='retroceso', on_delete=models.SET_NULL, null=True, blank=True)
    # Parámetros de dirección de giro izquierda
    giro_izq = models.ForeignKey(InfoTest, related_name='giro_izq', on_delete=models.SET_NULL, null=True, blank=True)
    # Parámetros de dirección de giro derecha
    giro_der = models.ForeignKey(InfoTest, related_name='giro_der', on_delete=models.SET_NULL, null=True, blank=True)
    # Activar perfil personalizado
    activo = models.BooleanField(_('activo'), default=False)
