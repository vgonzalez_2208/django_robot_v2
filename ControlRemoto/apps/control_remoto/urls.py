from django.urls import path

from apps.control_remoto.views.control_remoto.views import *

app_name = 'control_remoto'

urlpatterns = [
    path('control/', ControlRemoto.as_view(), name='control_remoto'),
    path('control_test/', AjustePID.as_view(), name='ajuste_pid'),
    path('nav_robot/', NavViewRobot.as_view(), name='nav_robot'),
]
