function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const csrftoken = getCookie('csrftoken');

// console.log(csrftoken);

async function global_fetch(action, parameters = new FormData) {
    if (!('action' in parameters)) {
        parameters.append('action', action);
    }
    let url = window.location.pathname;
    let response = await fetch(url, {method: 'POST', body: parameters, headers: {"X-CSRFToken": csrftoken}});
    return response.json();
}

function save_alert(info, icon='success', timer=1500) {
    Swal.fire({
        position: 'top-end',
        icon: icon,
        title: info,
        showConfirmButton: false,
        timer: timer
    })
}